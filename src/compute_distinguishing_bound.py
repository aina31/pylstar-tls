from typing import Dict
from itertools import combinations, product
import re
from pylstar.Letter import Letter
from pylstar.Word import Word
from pylstar.automata.State import State
from pylstar.automata.Transition import Transition
from pylstar.automata.Automata import Automata
import sys


def number_of_state(file):
    """
    Return the number of states a Mealy machine from its .automaton file
    """
    nb = 0
    with open(file, "r") as f:
        f.readline()
        for line in f:
            try:
                nb = max(int(line.split(",")[0]), nb)
            except:
                pass
    return nb + 1  # there are at least 1 state


def construct_automata(file):
    """
    Return a tuple composed of the automata object corresponding to the .automaton file, a dictionary of the letters
    used to browse the automata and the number of states of the automata
    """
    n = number_of_state(file)
    input_letters = {}
    states = {i: State(str(i)) for i in range(n + 1)}
    i = 0
    with open(file, "r") as f:
        f.readline()
        for line in f:
            try:
                elements = [k.strip() for k in re.split(",|\n", line)[:-1]]
                input_letters[elements[2]] = Letter(elements[2])
                t = Transition(
                    "Transition %d" % i,
                    states[int(elements[1])],
                    Letter(elements[2]),
                    Letter(elements[3]),
                )
                states[int(elements[0])].transitions.append(t)
                i += 1
            except:
                pass
            else:
                pass
    auto = Automata(initial_state=states[0], name="Automata to compute")
    return auto, input_letters, n


def print_states(list_of_states):
    """
    Convert a list of Letter objects into a list of str corresponding to te value of each Letter.
    """
    if len(list_of_states) == 0:
        return "Le chemin est vide"
    l = list(map(str, list_of_states))
    return l


def bdist_compute_from_file(file):
    """
    Return the value, the paths and the word corresponding to the B_dist of a Mealy Machine from its .automaton file
    """
    automate, in_letters, nb_state = construct_automata(file)
    input_letters = [Letter(u) for u in in_letters]
    return bdist_compute(automate, input_letters, nb_state)


def bdist_compute(automate: Automata, input_letters: dict, nb_state: int):
    """
    Return the value, the paths and the word corresponding to the B_dist of a Mealy Machine from an automata object
    b_dist : int : valeur de la borne de distinction de l'automate
    chemin_b_dist :  ([pylstar.automata.State] , [pylstar.automata.State]) : tuple contenant les chemins parcourus par
    une paire d'état vérifiant une longueur de b_dist
    mot_b_dist :  pylstar.Word : mot pour lequel le b_dist est atteint
    b_pairs : {str : pylstar.Word } : dictionnaire de l'ensemble des paires d'états pour lesquels le b_dist est atteint.
    la clé est de la forme "(state1, state2)"
    """
    states_pairs = list(combinations(automate.get_states(), 2))
    b_dist = 0
    chemin_b_dist = ("", "")
    mot_b_dist: Word = []
    b_pairs: Dict[str, Word] = {}

    if nb_state == 1:  # if the automata only has 1 state
        return 0, "", "", mot_b_dist, b_pairs

    else:
        for pair in states_pairs:
            break_var = False
            for i in range(1, nb_state):
                if break_var:
                    break
                words = list(product(input_letters, repeat=i))
                for w in words:
                    mot = Word([u for u in w if u != ""])
                    try:
                        chemin = list(
                            map(
                                lambda i, j: i + j,
                                (Word([Letter("")]), [pair[0]]),
                                automate.play_word(mot, pair[0]),
                            )
                        )  # [Word, [State]] pour le premier etat de la paire
                    except:
                        chemin = [mot, [pair[0]]]
                    try:
                        chemin_prime = list(
                            map(
                                lambda i, j: i + j,
                                (Word([Letter("")]), [pair[1]]),
                                automate.play_word(mot, pair[1]),
                            )
                        )  # [Word, [State]] idem pour le second etat
                    except:
                        chemin_prime = [mot, [pair[1]]]
                    if chemin[0] != chemin_prime[0]:
                        if b_dist <= i:
                            b_dist = i
                            mot_b_dist = mot
                            chemin_b_dist = (chemin[1], chemin_prime[1])
                            b_pairs["(%s, %s)" % (str(pair[0]), str(pair[1]))] = mot
                        break_var = True
                        break

            if break_var:
                continue

        b_pairs = {k: v for (k, v) in b_pairs.items() if len(v) == b_dist}
        return b_dist, chemin_b_dist[0], chemin_b_dist[1], mot_b_dist, b_pairs


if __name__ == "__main__":
    if len(sys.argv) != 2:
        exit("Usage : python bdist_compute.py file.automaton ")
    fichier = sys.argv[1]
    result = bdist_compute_from_file(fichier)
    print(f"Distinguishing Bound = {result[0]}")
    print(f"First path = {print_states(result[1])}")
    print(f"Second path = {print_states(result[2])}")
    print(f"Word triggering the bound = {result[3]}")
    print(f"State pairs reaching the bound = {result[4]}")
