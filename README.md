# pylstar-tls

[pylstar](https://github.com/gbossert/pylstar) applied to TLS using [scapy](https://github.com/secdev/scapy).

## Installation

There are two ways to use `pylstar-tls`

  * directly on your system (you will need to install Python3 and the depdendencies in requirements.txt)
  * in a Docker container (using the Dockerfile in the repo)


## Nano-tutorial

Once you have installed `pylstar-tls`, you can infer a server
listening at `SERVER:PORT` using the `tls13` scenario with the
following command:

    ./infer_server.py -R SERVER:PORT -S tls13

The output will be put into `/tmp/tls_inferer.automaton` but you can
specify another location using the `-o` option.


**TODO:** explain how to run client inference.


## Bugs and enhancements

Please, report any bug found by opening a ticket and/or by submiting a pull requests.


## Authors and acknowledgment

  * Aina Toky Rasoamanana
  * Olivier Levillain
  * Lorenzo Nadal Santa
  * Clément Parrsegny

This tool has been supported in part by the French ANR [GASP project](https://gasp.ebfe.fr) (ANR-19-CE39-0001).


## License

This software is licensed under the GPLv3 License. See the `COPYING.txt` file
in the top distribution directory for the full license text.
